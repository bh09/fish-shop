/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.scooter.service;

import hu.braininghub.scooter.dao.FishDao;
import hu.braininghub.scooter.dto.FishDto;
import hu.braininghub.scooter.dto.FishMapper;
import hu.braininghub.scooter.entity.Fish;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.ejb.Singleton;
import javax.inject.Inject;

/**
 *
 * @author ZsĂłfi
 */
@Singleton
public class FishService {
    @Inject
    private FishDao dao;
    
    private FishMapper mapper = new FishMapper();
    
    
    public List<FishDto> getAll () {   
        List<FishDto> listDto = new ArrayList<>();
        List<Fish> list = new ArrayList<>();
        list.addAll(dao.getAll());
        for (int i = 0; i < list.size()-1; i++) {
            listDto.add(mapper.mapEntityToDto(list.get(i)));
        }
        
        return listDto;
    
    }
    
    public void save(FishDto dto) {
        Fish entity = mapper.mapDtoToEntity(dto);
        dao.save(entity);
    }
    
    public void updateByQuantity(FishDto fishDto) {
        Long id = fishDto.getId();  
       Optional<Fish> fish = dao.get(id);
        
       if (fish.isPresent()) {
           dao.update(fish.get());
       }
        
    }
}
