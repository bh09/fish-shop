package hu.braininghub.scooter.service;

import hu.braininghub.scooter.dao.OrderDao;
import hu.braininghub.scooter.dto.OrderDto;
import hu.braininghub.scooter.dto.OrderMapper;
import hu.braininghub.scooter.entity.Order;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Singleton;
import javax.inject.Inject;

/**
 *
 * @author horvathbzs
 */

@Singleton
public class OrderService {
    
    @Inject
    private OrderDao dao;
    
    private OrderMapper orderMapper = new OrderMapper();
    
    public List<OrderDto> getAll() {
        List<OrderDto> listDto = new ArrayList<>();
        List<Order> list = new ArrayList<>();
        list.addAll(dao.getAll());
        for (int i = 0; i <  list.size() - 1; i++) {
            listDto.add(orderMapper.mapEntityToDto(list.get(i)));
        }
        
        return listDto;
    }
    
    public void save(OrderDto dto) {
        Order order = orderMapper.mapDtoToEntiy(dto); 
        dao.save(order);
    }
}
