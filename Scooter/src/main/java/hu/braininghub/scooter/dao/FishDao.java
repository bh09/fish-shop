/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.scooter.dao;

import hu.braininghub.scooter.entity.Fish;
import java.util.List;
import java.util.Optional;
import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Zsófi
 */
@Stateful
@LocalBean
public class FishDao {

    @PersistenceContext(unitName = "FishPU")
    EntityManager em;
    
    public List<Fish> getAll() {
        Query q = em.createQuery("Select u from Fish u", Fish.class);
        return q.getResultList();
    }
    

    public void save(Fish fish) {
        em.persist(fish);
    }
    
  public void update(Fish fish) {
        em.merge(fish);
    }
  
  public Optional<Fish> get(Long id) {
      return Optional.ofNullable(em.find(Fish.class, id));
  }

}
