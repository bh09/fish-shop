package hu.braininghub.scooter.dao;

import hu.braininghub.scooter.entity.Order;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author horvathbzs
 */
public class OrderDao {
    
    @PersistenceContext(unitName = "FishPU")
    EntityManager em;
    
    public List<Order> getAll() {
        Query q = em.createQuery("Select o from Order o", Order.class);
        return q.getResultList();
    }
    
    public Optional<Order> get(Long id) {
        return Optional.ofNullable(em.find(Order.class, id));
    }
    
    public void save(Order order) {
        em.persist(order);
    }
    
    public void update(Order order) {
        em.merge(order);
    }
    
    
    
    
}
