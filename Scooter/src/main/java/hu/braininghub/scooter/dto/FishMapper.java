/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.scooter.dto;

import hu.braininghub.scooter.entity.Fish;
import hu.braininghub.scooter.entity.FishTyp;

/**
 *
 * @author Zsófi
 */
public class FishMapper {
    
   public Fish mapDtoToEntity(FishDto dto) {
       Fish entity = new Fish() ;
       entity.setFishtyp(dto.getFishtyp());
       entity.setId(dto.getId());
       entity.setPrice(dto.getPrice());
       entity.setQuantity(dto.getQuantity());
       
       return entity;
   } 
   
   
   public FishDto mapEntityToDto(Fish entity) {
       FishDto dto = new FishDto() ;
       
       
       dto.setFishtyp(entity.getFishtyp());
       dto.setId(entity.getId());
       dto.setPrice(entity.getPrice());
       dto.setQuantity(entity.getQuantity());
       
       return dto;
   } 
}
