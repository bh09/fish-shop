package hu.braininghub.scooter.dto;

import hu.braininghub.scooter.entity.FishTyp;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author horvathbzs
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderDto {
    
    private Long id;
    private String email;   
    private FishTyp fishtyp;   
    private LocalDateTime time;
    
}
