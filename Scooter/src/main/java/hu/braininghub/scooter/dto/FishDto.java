/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.scooter.dto;

import hu.braininghub.scooter.entity.FishTyp;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Zsófi
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FishDto {
     private Long id;
    private int quantity;
    private int price;
    private FishTyp fishtyp; 
}
