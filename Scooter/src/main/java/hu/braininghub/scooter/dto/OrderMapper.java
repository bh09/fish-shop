package hu.braininghub.scooter.dto;

import hu.braininghub.scooter.entity.Order;

/**
 *
 * @author horvathbzs
 */
public class OrderMapper {
    
    public Order mapDtoToEntiy(OrderDto dto) {
        
        Order entity = new Order();
        
        entity.setId(dto.getId());
        entity.setEmail(dto.getEmail());
        entity.setFishtyp(dto.getFishtyp());
        entity.setTime(dto.getTime());
        
        return entity;
    }
    
    public OrderDto mapEntityToDto(Order entity) {
        
        OrderDto dto = new OrderDto();
        
        dto.setId(entity.getId());
        dto.setEmail(entity.getEmail());
        dto.setFishtyp(entity.getFishtyp());
        dto.setTime(entity.getTime());
        
        return dto;
    }
    
    
    
}
