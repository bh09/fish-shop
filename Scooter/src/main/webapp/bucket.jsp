<%-- 
    Document   : bucket
    Created on : 2019.07.28., 17:09:17
    Author     : Misi
--%>

<%@page contentType="text/html" pageEncoding="windows-1250"%>
<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1250">
        <title>Rendel�sek</title>
    </head>
    <body>
        <%@include file="/assets/navbar.jsp" %>
        <h1>Kos�r tartalma</h1>
        <br>
        <table>
            <thead>
                <tr>
                    <th scope="col">Hal fajt�ja</th>
                    <th scope="col">�r</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${sessionScope.bucket.orderList}" var="fish">
                    <tr>
                        <td>${fish.fishtyp}</td>
                        <td>${fish.price}</td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
        <form method="POST">
            <input type="email" placeholder="e-mail" class="form-control" name="email"/>
            <input type="submit" placeholder="V�s�rl�s" class="form-control">
            
        </form>
    </body>
</html>
