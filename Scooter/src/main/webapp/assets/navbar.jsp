<%-- 
    Document   : navbar
    Created on : 2019.07.28., 16:38:53
    Author     : Misi
--%>

<%@page contentType="text/html" pageEncoding="windows-1250"%>
<!DOCTYPE html>
<!-- As a link -->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="#">Terméklista<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Kosár</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Rendelések</a>
            </li>
        </ul>
    </div>
</nav>
