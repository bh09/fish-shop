<%-- 
    Document   : products
    Created on : 2019.07.28., 17:10:40
    Author     : Misi
--%>

<%@page contentType="text/html" pageEncoding="windows-1250"%>
<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1250">
        <title>Scooter-�rucikkek</title>
    </head>
    <body>
        <%@include file="/assets/navbar.jsp" %>
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">Hal fajt�ja</th>
                    <th scope="col">Db</th>
                    <th scope="col">�r</th>
                    <th scope="col">Rendel�s</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${fishlist}" var="fish">
                    <tr>
                        <td>${fish.fishtyp}</td>
                        <td>${fish.quantity}</td>
                        <td>${fish.price}</td>
                        <td><button name="${fish.fishtyp}" value="Hozz�ad" /> </td>
                    </tr>

                </c:forEach>
            </tbody>
        </table>
    </body>
</html>
